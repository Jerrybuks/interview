import React, { useReducer } from 'react';
import UserContext from './userContext';
import userReducer from './userReducer';
import { EDIT_USER, GET_USER, DELETE_USER,USER_ERROR } from '../types';

export default function UserState(props) {
	const initialState = {
		user: [],
		status: null,
        error: null,
        loading: true
	};

	const [ state, dispatch ] = useReducer(userReducer, initialState);

	const getUser = () => {
		fetch('https://randomuser.me/api/')
			.then((resp) => resp.json()) // Transform the data into json
			.then(function(data) {
				console.log(data.results);
				dispatch({
					type: GET_USER,
					payload: data.results
				});
			})
			.catch((error) =>
				dispatch({
					type: USER_ERROR,
					payload: error
				})
			);
	};
	const editUser =  (user) => {
		console.log(user)
		try {
			
			dispatch({
				type: EDIT_USER,
				payload: ""
			});
		} catch (error) {
			dispatch({
				type: USER_ERROR,
				payload: ""
			});
		}
	};
	const deleteUser =  (id) => {
		try {
			dispatch({
				type: DELETE_USER,
				payload: id
			});
		} catch (error) {
			dispatch({
				type: USER_ERROR,
				payload: error
			});
		}
	};

	// const clearStatus = () => { dispatch({ type: CLEAR_STATUS})}
	return (
		<UserContext.Provider
			value={{
				user: state.user,
				error: state.error,
                books: state.books,
                loading: state.loading,
				// status: state.status,
				getUser,
				editUser,
				deleteUser,
			}}
		>
			{props.children}
		</UserContext.Provider>
	);
}
