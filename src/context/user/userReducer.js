import { EDIT_USER, GET_USER, DELETE_USER } from "../types";
export default  (state, action) => {
    console.log(state.user,action.payload)
    switch (action.type) {
        case GET_USER:
            console.log(action.payload)
            return ({...state, user : [...state.user,...action.payload], loading: false})
        case EDIT_USER:
            return ({...state, books : action.payload})
        case DELETE_USER:
            const allUsers = state.user.filter(user =>  user.login.uuid !== action.payload);
            return ({...state, user : allUsers})
        default:
            return state;
    }
}