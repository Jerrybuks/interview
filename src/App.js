import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/home/Home';
import Users from './components/user/User';
import UserState from './context/user/UserState';

import './App.scss';
function App() {
	return (
		<UserState>
			<Router>
				<Switch>
					<Route exact path="/" component={Home} />
					<Route exact path="/users" component={Users} />
				</Switch>
			</Router>
		</UserState>
	);
}

export default App;
