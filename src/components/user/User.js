import React, { useEffect, useState, useContext } from 'react';
import UserContext from '../../context/user/userContext';
import Card from '../../common/card/Card';
import Spinner from '../../common/spinner/Spinner';
import Modal from '../../common/modal/Modal'
import edit from '../../assets/edit.svg';
import deleteIcon from '../../assets/delete.svg';
import EditUser from './EditUser';

export default function User() {
	const userContext = useContext(UserContext);
	const { getUser, user, loading, deleteUser } = userContext;
	const [ state, setState ] = useState({ isModalOpen: false, currentUser: {}});
	const { isModalOpen, currentUser } = state;

	useEffect(
		
		() => {
			const abortController = new AbortController();
			getUser();
			return () => {
				abortController.abort();
			  };
		},
		[ currentUser]
	);
	console.log(user);

	const closeModal = () => {
		setState({
			...state,
			isModalOpen: false
		});
	};
	
	// open modal (set isModalOpen, false)
	const openModal = (user) => {
		setState({
			...state,
			isModalOpen: true,
			currentUser: user
		});
	};

	const handleClick = () => {
		getUser();
	};
	return (
		<div className="user">
			<div className="user-container">
				{!loading ? (
					<div>
						{user.map(
							(
								curUser,
								index
							) => {
								const {
									name: { first, last },
									login: { username, uuid },
									picture: { medium },
									location: { street: { number, name }, city, country }
								} = curUser
								return (
								<div key={index} className="user--card">
									<Card>
										<img src={medium} alt="profile" className="user--card-img" />
										<div>
											{first} {last}
											<img src={edit} alt="edit" className="user-edit" />
										</div>
										<div className="user--card-username">{username}</div>
										<div>
											{number} {name} {city}, {country}
										</div>
										<img
											src={deleteIcon}
											alt="delete"
											className="user-del"
											onClick={() => deleteUser(uuid)}
										/>
									</Card>
									<Modal isModalOpen={isModalOpen} closeModal={closeModal}>
											<EditUser edit={true} userData={state.currentUser} onCancel={closeModal} />
									</Modal>
								</div>
							)}
						)}
						<div className="user--add-button" onClick={handleClick}>
							+
						</div>
					</div>
				) : (
					<Spinner />
				)}
			</div>
		</div>
	);
}
