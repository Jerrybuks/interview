import React, { useState, useEffect, useContext } from 'react'

import UserContext from '../../context/user/userContext'

export default function AddInventory({userData, onCancel}) {
    const userContext = useContext(UserContext)
    console.log(userData)
    const { editUser,user } = userContext;
    useEffect(() => {
        
    }, [userData])
    let initialState = {};

    console.log(userData)
        if(userData.name){
            initialState = {
                first: userData.name.first,
                last: userData.name.last,
                username: userData.login.username,
                number:  userData.location.street.number,
                name: userData.location.street.name,
                city:  userData.location.city,
                country:  userData.location.country,
            }
        } else {
            initialState = {
                first: "",
                last:"",
                username: "",
                number:  "",
                name: "",
                city:  "",
                country:  "",
            }
        }
    
    const [state, setState] = useState(initialState)
    onchange = (e) => {
        setState({ ...state, [e.target.name]: e.target.value })
    }

    onsubmit = (e) => {
         e.preventDefault()
        const user = {
            ...userData,
            name:{
                first : state.first,
                last: state.last
            },
            login:{
                username : state.username
            },
        }
       editUser(user)
        onCancel();
        
    }
    const { first, last, username, number : num, name, city, country } = state
    return (
        <div className="form-inventory">
            <form className="utils-mg-tp-small">
                <h3 className="form-inventory__header">Edit User</h3>
                <fieldset className="form-inventory__body">
                    <div>
                        <label htmlFor="first"> First Name :</label>
                        <input id="first" name="first" type="text" value={first} onChange={onchange} required />
                    </div>
                    <div>
                        <label htmlFor="last">Last Name  : </label>
                        <input id="last" name="last" type="text" value={last} onChange={onchange} required />
                    </div>
                    <div>
                        <label htmlFor="username"> Username :</label>
                        <input id="username" name="username" type="text" value={username} onChange={onchange} required />
                    </div>
                                       <div>
                        <label htmlFor="num"> Street Number  :</label>
                        <input id="num" name="num" type="num" min="1" value={num} onChange={onchange} required />
                    </div>
                    <div>
                        <label htmlFor="name"> name :</label>
                        <input id="name" name="name" type="text" value={name} onChange={onchange} required />
                    </div>
                    <div>
                        <label htmlFor="city"> city :</label>
                        <input id="city" name="city" type="text" value={city} onChange={onchange} required />
                    </div>
                    <div className="align-center " ><input className="btn btn--primary" type="submit" /></div>
                </fieldset>
            </form>
        </div>
    )
}
